const {Router} = require('express')
const bcrypt = require ('bcryptjs')
const config = require('config')
const jwt = require('jsonwebtoken')
const {check, validationResult} = require('express-validator')
const user = require('../models/User')
const User = require('../models/User')
const router = Router()

// /api/auth/register
router.post(
    '/register',
    [
        check('email', 'Nepravilniy email').isEmail(),
        check('password', 'Минимальная длинна пароля 6 символов')
        .isLength({min:6})
    ],
    async (req, res) => {
    try{
        
        const errors = validationResult(req)

        if (!errors.isEmpty()) {
            return res.status(400).json({
                errors: errors.array(),
                message: 'Некоректные данные при регистрации'
            })
        }


        const {email, password} = req.body

        const candidate = await User.findOne({email})

        if (candidate) {
           return res.status(400).json({message: 'Uzhe est takoi USER!'})
        }

        const hashedPassword = await bcrypt.hash(password, 12)
        const user = new User({email, password: hashedPassword})

        await user.save()

        res.status(201).json({message: 'user created!!!'})
    } catch(e) {
        res.status(500).json({message: 'Smth WRONG, try again!'})
    }
})
// /api/auth/login
router.post(
    '/login',
    [
        check('email', 'Enter normal email').normalizeEmail().isEmail,
        check('password', 'Enter password').exists()
    ],
     async (req, res) => {
        try{
            const errors = validationResult(req)
    
            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: 'Некоректные данные при входе в систему'
                })
            }

            const {email, password} = req.body

            const user = await User.findOne({email})

            if(!user) {
                return res.status(400).json({message: 'user not find'})
            }

            const isMatch = await bcrypt.compare(password, user.password)

            if(!isMatch) {
                return res.status(400).json({message: 'Not right password. try again'})
            }

            const token = jwt.sign(
                {userId: user.id},
                config.get('jwtSecret'),
                {expiresIn: '1h'}
            )

            res.json({token, userId: user.id})
    
        } catch(e) {
            res.status(500).json({message: 'Smth WRONG, try again!'})
        }
})



module.exports = router